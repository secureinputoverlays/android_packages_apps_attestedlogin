/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sobel.android.attestedloginapp;

import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.sobel.android.attestedhttpservice.AttestedPost;
import com.sobel.android.attestedhttpservice.AttestedPostResponse;
import com.sobel.android.hiddenbufferservice.HiddenContentHandle;
import com.sobel.android.secureinputservice.SecureInputManager;
import com.sobel.android.secureinputservice.SecureInputView;
import com.sobel.android.secureinputservice.SecureInputViewListener;


/**
 * AttestedLoginActivity
 */
public class AttestedLoginActivity extends Activity {
    /**
     * Called with the activity is first created.
     */

	private static final String TAG = "AttestedLoginActivity";
	
	private static final String URL = "http://10.0.2.2:9876/login";
	
	// http://www.regular-expressions.info/email.html
	private static Pattern emailRegex = Pattern.compile(
			"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$",
			Pattern.CASE_INSENSITIVE
	);
	
	private static Pattern passwordRegexUppercase = Pattern.compile("^.*?[A-Z].*$");
	private static Pattern passwordRegexLowercase = Pattern.compile("^.*?[a-z].*$");
	private static Pattern passwordRegexNumber = Pattern.compile("^.*?[0-9].*$");
	private static Pattern passwordRegexSpecial = Pattern.compile("^.*?[!^$&%*#@].*$");
	
	private LinearLayout mRootLayout;
	
	private SecureInputView mEmailInput;
	private SecureInputView mPasswordInput;
	
	private TextView mEmailStatus;
	private TextView mPasswordStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRootLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.attestedlogin_activity, null);
        setContentView(mRootLayout);

        mEmailStatus = (TextView)findViewById(R.id.emailStatus);
        errorStatus(mEmailStatus, "Enter your email:");
        mEmailInput = (SecureInputView)findViewById(R.id.emailInput);
        mEmailInput.setListener(new SecureInputViewListener() {

			@Override
			public void onEditStart() {}

			@Override
			public void onEditStop() {
		    	HiddenContentHandle emailContent = mEmailInput.getHiddenContentHandle();
				validateEmail(emailContent);
			}
        });
        
        mPasswordStatus = (TextView)findViewById(R.id.passwordStatus);
        errorStatus(mPasswordStatus, "Enter your password:");
        mPasswordInput = (SecureInputView)findViewById(R.id.passwordInput);
        mPasswordInput.setListener(new SecureInputViewListener() {

			@Override
			public void onEditStart() {}

			@Override
			public void onEditStop() {
				HiddenContentHandle passwordContent = mPasswordInput.getHiddenContentHandle();
				validatePassword(passwordContent);
			}
        	
        });
    }
    
    public void doSubmit(View v) {
    	HiddenContentHandle emailContent = mEmailInput.getHiddenContentHandle();
    	HiddenContentHandle passwordContent = mPasswordInput.getHiddenContentHandle();

    	if (!validateEmail(emailContent)) {
    		Toast.makeText(getApplicationContext(), "Invalid Email", Toast.LENGTH_SHORT).show();
    		return;
    	}
    	if (!validatePassword(passwordContent)) {
    		Toast.makeText(getApplicationContext(), "Invalid Password", Toast.LENGTH_SHORT).show();
    		return;
    	}
    	AttestedPost req = new AttestedPost.Builder()
    		.url(URL)
    		.addHeader("X-app-id", "foobar")
    		.addParam("email", emailContent)
    		.addParam("password", passwordContent)
    		.build();
    	
    	req.perform(new AttestedPost.ResponseListener() {
			@Override
			public void onResponse(AttestedPostResponse r) {
				String text;
				if (r.getCode() != 200) {
					text = "ERROR: " + r.getStatusLine();
				} else {
					text = "Success! " + new String(r.getBody());
				}
				Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
			}
		}, new AttestedPost.ErrorListener() {
			@Override
			public void onError(String msg) {
				String text = "TRANSPORT ERROR: " + msg;
				Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
			}
		});
    	
    	
    }
    
    private void errorStatus(TextView tv, String s) {
    	tv.setTextColor(Color.RED);
    	tv.setText(s);
    }
    
    private void okStatus(TextView tv, String s) {
    	tv.setTextColor(Color.argb(0xFF, 0x15, 0x80, 0x00));
    	tv.setText(s);
    }
    
    private boolean validateEmail(HiddenContentHandle emailContent) {
    	if (!emailContent.matchesPattern(emailRegex)) {
    		errorStatus(mEmailStatus, "Does not match email regex");
    		return false;
    	} else {
    		okStatus(mEmailStatus, "Valid Email");
    		return true;
    	}
    }
    
    private boolean validatePassword(HiddenContentHandle passwordContent) {
    	int length = passwordContent.getLength();
    	if (length < 6 || length > 16) {
    		errorStatus(mPasswordStatus, "Length must be in [6, 16]");
    		return false;
    	}
    	boolean hasUpper = passwordContent.matchesPattern(passwordRegexUppercase);
    	if (!hasUpper) {
    		errorStatus(mPasswordStatus, "Must have an uppercase letter");
    		return false;
    	}
    	boolean hasLower = passwordContent.matchesPattern(passwordRegexLowercase);
    	if (!hasLower) {
    		errorStatus(mPasswordStatus, "Must have a lowercase letter");
    		return false;
    	}
    	boolean hasNumber = passwordContent.matchesPattern(passwordRegexNumber);
    	if (!hasNumber) {
    		errorStatus(mPasswordStatus, "Must have a number");
    		return false;
    	}
    	boolean hasSpecial = passwordContent.matchesPattern(passwordRegexSpecial);
    	if (!hasSpecial) {
    		errorStatus(mPasswordStatus, "Must have special character (^&$%*#@!)");
    		return false;
    	}

    	okStatus(mPasswordStatus, "OK");
    	return true;
    }
    
}

